package myprojects.automation.assignment5.model;

import com.github.javafaker.Commerce;
import com.github.javafaker.Faker;

import java.text.ParseException;
import java.util.Locale;
import java.util.Random;

/**
 * Hold Product information that is used among tests.
 */
public class ProductData {
    private String name;
    private int qty;
    private float price;

    public ProductData(String name, int qty, float price) {
        this.name = name;
        this.qty = qty;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getQty() {
        return qty;
    }

    public float getPrice() {
        return price;
    }

    /**
     * @return New Product object with random name, quantity and price values.
     */
    public static ProductData generate() throws ParseException {
        Random random = new Random();
        Locale ru = new Locale("ru", "ru");
        Commerce gen = new Faker(ru).commerce();

        return new ProductData(
                gen.productName(),
                random.nextInt(100) + 1,
                Float.parseFloat(gen.price().replace(",", ".")));
    }

    @Override
    public boolean equals(Object ob) {
        if (ob == null) return false;
        if (ob.getClass() != getClass()) return false;

        ProductData other = (ProductData)ob;

        return name.equals(other.getName())
                && qty == other.getQty()
                && Float.compare(price, other.getPrice()) == 0;
    }

    @Override
    public String toString() {
        return String.format("%s {'%s', %.2f, %d}", getClass().getSimpleName(), name, price, qty);
    }
}
