package myprojects.automation.assignment5.utils;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
//import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.SkipException;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class DriverFactory {
    /**
     * @param resourceName The name of the resource
     * @return Path to resource
     */
    private static String getResource(String resourceName) {
        URL res = DriverFactory.class.getResource(resourceName);
        if(res == null) //No ie for linux
            throw new SkipException("No driver: " + resourceName);

        File f = new File(res.getFile());
        f.setExecutable(true);
        return f.getPath();
    }

    public static DesiredCapabilities getCaps(String browser) {
        DesiredCapabilities dc = DesiredCapabilities.chrome();
        switch (browser) {
            case "firefox":
                return DesiredCapabilities.firefox();
            case "ie":
            case "internet explorer":
                dc = DesiredCapabilities.internetExplorer();
                dc.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
                dc.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
                return dc;
            case "phantomjs":
                dc = DesiredCapabilities.phantomjs();
                dc.setCapability("Platform", Platform.ANY);
                return dc;
            case "android":
                Map<String, String> me = new HashMap<>();
                me.put("deviceName", "Galaxy S5");

                Map<String, Object> opt = new HashMap<>();
                opt.put("mobileEmulation", me);

                dc.setCapability(ChromeOptions.CAPABILITY, opt);
            case "chrome":
            default:
                return dc;
        }
    }

    /**
     *
     * @param browser Driver type to use in tests.
     * @return New instance of {@link WebDriver} object.
     */
    public static WebDriver initDriver(String browser) {
        String suffix = System.getProperty("os.name").startsWith("Win") ? ".exe" : "";
        DesiredCapabilities dc = getCaps(browser);
        switch (browser) {
            case "firefox":
                System.setProperty(
                        "webdriver.gecko.driver",
                        getResource("/geckodriver" + suffix));
                return new FirefoxDriver(dc);
            case "ie":
            case "internet explorer":
                System.setProperty(
                        "webdriver.ie.driver",
                        getResource("/IEDriverServer" + suffix));
                return new InternetExplorerDriver(dc);
            case "phantomjs":
                System.setProperty(
                        "phantomjs.binary.path",
                        getResource("/phantomjs" + suffix));
                return new PhantomJSFixedDriver(dc);
            case "android":
            case "chrome":
            default:
                System.setProperty(
                        "webdriver.chrome.driver",
                        getResource("/chromedriver" + suffix));
                return new ChromeDriver(dc);
        }
    }

    /**
     *
     * @param browser Remote driver type to use in tests.
     * @param gridUrl URL to Grid.
     * @return New instance of {@link RemoteWebDriver} object.
     */
    public static WebDriver initDriver(String browser, String gridUrl) throws MalformedURLException {
        return new RemoteWebDriver(new URL(gridUrl), getCaps(browser));
    }
}
