package myprojects.automation.assignment5;


//import com.google.common.base.Predicate;
import myprojects.automation.assignment5.model.ProductData;
import myprojects.automation.assignment5.utils.DataConverter;
import myprojects.automation.assignment5.utils.Properties;
import myprojects.automation.assignment5.utils.logging.CustomReporter;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;

import java.util.function.*;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private WebDriver driver;
    private JavascriptExecutor js;
    private WebDriverWait wait;

    //shop main page
    private final By allLink = By.className("all-product-link");

    //product list shop page
    private final By productLink = By.xpath("//h1[contains(@class, 'product-title')]/a");

    //product shop page
    private final By detailsTab = By.xpath("//a[@href='#product-details']");
    private final By nameEl = By.cssSelector("ol > li:last-child");
    private final By priceSel = By.xpath("//span[@itemprop='price']");
    private final By amountSel = //By.cssSelector("div.product-quantities > span");
            By.xpath("//div[@class='product-quantities']/span");
    private final By availSel = By.id("product-availability");

    public GeneralActions(WebDriver driver) {
        this.driver = driver;
        js = (JavascriptExecutor) driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void openRandomProduct() {
        driver.get(Properties.getBaseUrl());

        WebElement allProducts = driver.findElement(allLink);
        scrollTo(allProducts);
        wait.until(ExpectedConditions.elementToBeClickable(allProducts))
                .click();

        List<WebElement> products = driver.findElements(productLink);
        int i = (new Random().nextInt()) % products.size();

        CustomReporter.log("Selected element: " + i + " from total " + products.size());
        WebElement selectedProduct = products.get(Math.abs(i));
        scrollTo(selectedProduct);
        //clickJS(products.get(i));
        wait.until(ExpectedConditions.elementToBeClickable(selectedProduct))
                .click();

    }

    /**
     * Extracts product information from opened product details page.
     *
     * @return
     */
    public ProductData getOpenedProductInfo() {
        CustomReporter.logAction("Get information about currently opened product");

        waitForContentLoad();


        String name   = driver.getTitle(); //driver.findElement(nameEl).getText();
        String price  = driver.findElement(priceSel).getText().trim();

        String amount;
        String status = driver.findElement(availSel).getText().trim();
        if(status.toLowerCase().contains("нет в наличии"))
           amount = "0";
        else {
            scrollTo(driver.findElement(detailsTab)).click();

            WebElement amountEl = driver.findElement(amountSel);
            scrollTo(amountEl);
            wait.until(ExpectedConditions.visibilityOf(amountEl));

            amount = amountEl.getText();
        }

        return new ProductData(name,
                DataConverter.parseStockValue(amount),
                DataConverter.parsePriceValue(price));
    }

    public void clickJS(WebElement e) {
        js.executeScript("arguments[0].click()", e);
    }

    private void waitForContentLoad() {
        Function<WebDriver, Boolean> isReady = d -> ((JavascriptExecutor) d)
                .executeScript("return document.readyState").equals("complete");
        wait.until(isReady);
    }

    public WebElement scrollTo(WebElement e){
        js.executeScript("arguments[0].scrollIntoView(true);", e);
        return e;
    }

    public boolean isMobileVersion() {
        return (boolean) js.executeScript("return $('#menu-icon').is(':visible')");
    }
}
