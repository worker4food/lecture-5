package myprojects.automation.assignment5.tests;

import com.github.javafaker.Address;
import com.github.javafaker.Faker;
import myprojects.automation.assignment5.BaseTest;
import myprojects.automation.assignment5.model.ProductData;
import myprojects.automation.assignment5.utils.DataConverter;
import myprojects.automation.assignment5.utils.Properties;
import myprojects.automation.assignment5.utils.logging.CustomReporter;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.Locale;
import java.util.Scanner;

public class PlaceOrderTest extends BaseTest {

    @Test
    public void checkSiteVersion() {
        driver.get(Properties.getBaseUrl());
        Assert.assertEquals(isMobileTesting, actions.isMobileVersion());
    }

    @Test
    public void createNewOrder() {
        ProductData p = null;
        int amount = 0;

        while (amount == 0) {
            // open random product
            actions.openRandomProduct();

            // save product parameters
            p = actions.getOpenedProductInfo();
            CustomReporter.log("Selected product: " + p);

            amount = p.getQty();
            if(amount == 0)
                CustomReporter.log("No products left!");
        }

        String productUrl = driver.getCurrentUrl();

        CustomReporter.log("Product URL: " + productUrl);
        CustomReporter.log("\t" + p);

        // add product to Cart and validate product information in the Cart
        actions.clickJS(driver.findElement(By.xpath("//button[@data-button-action='add-to-cart']")));

        actions.clickJS(driver.findElement(By.cssSelector("a.btn-primary")));

        String name  = driver.findElement(By.cssSelector("div.product-line-info > a.label")).getText();
        String price = driver.findElement(By.cssSelector("#cart-subtotal-products > span.value")).getText();
        String qty   = driver.findElement(By.cssSelector("#cart-subtotal-products > span.label")).getText();

        Assert.assertEquals(p.getName(), name);
        Assert.assertEquals(p.getPrice(), DataConverter.parsePriceValue(price));
        Assert.assertEquals(1, DataConverter.parseStockValue(qty));

        // proceed to order creation, fill required information
        actions.clickJS(driver.findElement(By.cssSelector("a.btn-primary")));

        driver.findElement(By.name("id_gender")).click();

        Locale lc = new Locale("ru", "ru");
        Faker gen = new Faker();
        Address addr = gen.address();


        actions.scrollTo(driver.findElement(By.name("firstname")))
                .sendKeys(addr.firstName());

        actions.scrollTo(driver.findElement(By.name("lastname")))
                .sendKeys(addr.lastName());

        actions.scrollTo(driver.findElement(By.name("email")))
                .sendKeys(gen.internet().emailAddress());

        actions.scrollTo(driver.findElement(By.xpath("//button[@data-link-action = 'register-new-customer']")))
                .click();
        CustomReporter.log("Basic info filled");

        actions.scrollTo(driver.findElement(By.name("address1")))
                .sendKeys(addr.streetAddress());

        actions.scrollTo(driver.findElement(By.name("city")))
                .sendKeys(addr.cityName());

        actions.scrollTo(driver.findElement(By.name("postcode")))
                .sendKeys(addr.zipCode().substring(0, 5));

        actions.scrollTo(driver.findElement(By.name("confirm-addresses")))
                .click();
        CustomReporter.log("Address info filled");

        actions.scrollTo(driver.findElement(By.name("confirmDeliveryOption")))
                .click();
        CustomReporter.log("Delivery options filled");

        // place new order and validate order summary
        actions.scrollTo(driver.findElement(By.xpath("//label[@for='payment-option-2']")))
                .click();

        actions.scrollTo(driver.findElement(By.id("conditions_to_approve[terms-and-conditions]")))
                .click();

        actions.scrollTo(driver.findElement(By.xpath("//div[@id='payment-confirmation']//button")))
                .click();

        String result = driver.findElement(By.cssSelector("h3.h1.card-title")).getText();
        Assert.assertTrue(result.toLowerCase().contains("ваш заказ подтверждён"), "Order not confirmed");
        CustomReporter.log("Order placed and confirmed");

        String orderName = driver.findElement(By.cssSelector("div.row > div.details")).getText();
        String orderPrice = driver.findElement(By.cssSelector("div.row > div.qty > div.row > div.text-xs-left")).getText();
        String orderAmount = driver.findElement(By.cssSelector("div.row > div.qty > div.row > div.col-xs-2")).getText();

        Assert.assertTrue(orderName.startsWith(p.getName()));
        Assert.assertEquals(p.getPrice(), DataConverter.parsePriceValue(orderPrice));
        Assert.assertEquals(1, DataConverter.parseStockValue(orderAmount));

        // check updated In Stock value
        driver.get(productUrl);
        ProductData updatedP = actions.getOpenedProductInfo();

        CustomReporter.log("Qty before: " + p.getQty());
        CustomReporter.log("Qty after: " + updatedP.getQty());

        Assert.assertTrue(p.getQty() - updatedP.getQty() == 1, "Amounts check failed");
    }

}
